package com.jae.tipsy.tax;

import android.content.Context;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.HandlerThread;
import com.jae.tipsy.util.CachedPropertyModel;

import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: David
 * Date: 4/20/13
 * Time: 8:12 AM
 * DefaultTax serves as a model for the tax selector and provides default tax information.
 */
public class DefaultTax extends CachedPropertyModel {

    private LocationManager locationManager;
    private Geocoder gcd;

    private LocationListener listener;

    private HandlerThread handlerThread;

    public DefaultTax(Context context) {
        gcd = new Geocoder(context, Locale.US);
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);


        handlerThread = new HandlerThread("background-handler");
        handlerThread.start();

        //Listen for Location Updates
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                new LongOperation().execute(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onProviderEnabled(String provider) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onProviderDisabled(String provider) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        };
        requestLocationUpdates();

    }

    public void requestLocationUpdates() {
        if (locationManager != null && locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && listener != null && handlerThread != null) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60000, 10, listener, handlerThread.getLooper());
        }
    }

    private void update(Location location){

    }

    private class LongOperation extends AsyncTask<Location, Void, String> {
        @Override
        protected String doInBackground(Location... params) {
            update(params[0]);
            return "";
        }
    }
}
