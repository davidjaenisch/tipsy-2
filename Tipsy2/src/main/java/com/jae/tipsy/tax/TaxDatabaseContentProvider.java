package com.jae.tipsy.tax;

import android.net.Uri;
import com.jae.tipsy.util.DatabaseContentProvider;

/**
 * Created by David on 11/8/13.
 */
public class TaxDatabaseContentProvider extends DatabaseContentProvider {

    private static final String AUTHORITY = "taxContentProvider";
    private static final String DATABASE_NAME = "zipsf";
    private static final int DATABASE_VERSION = 1;
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public TaxDatabaseContentProvider() {
        super(AUTHORITY, DATABASE_NAME, DATABASE_VERSION);
    }
}
