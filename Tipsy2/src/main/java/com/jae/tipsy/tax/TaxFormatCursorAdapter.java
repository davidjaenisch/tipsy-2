package com.jae.tipsy.tax;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.jae.tipsy.util.QueryLoader;

import java.text.NumberFormat;

/**
 * Created by David on 11/17/13.
 */
public class TaxFormatCursorAdapter extends SimpleCursorAdapter {

    private static NumberFormat format = NumberFormat.getPercentInstance();

    private QueryLoader queryLoader;

    static {
        format.setMaximumFractionDigits(4);
    }

    public TaxFormatCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
    }

    public TaxFormatCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }


    @Override
    public void setViewText(TextView v, String text) {
        if(isPercent(text)){
            text = format.format(Double.valueOf(text));

        }
        super.setViewText(v, text);
    }

    public static boolean isPercent(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
            return d < 1;
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
    }
}
