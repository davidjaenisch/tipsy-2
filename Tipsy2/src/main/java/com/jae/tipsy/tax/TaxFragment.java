package com.jae.tipsy.tax;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import com.jae.tipsy.R;
import com.jae.tipsy.util.Query;
import com.jae.tipsy.util.QueryLoader;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * Created by David on 10/27/13.
 */
public class TaxFragment extends RoboFragment {

    @InjectView(R.id.gpsList)
    private ListView gpsList;

    @InjectView(R.id.searchList)
    private ListView searchList;

    @InjectView(R.id.searchView)
    private SearchView searchView;

    private QueryLoader gpsLoader;
    private QueryLoader searchLoader;

    private static final int GPS_ID = 50;
    private static final int SEARCH_ID = 51;

    private Context context;

    public TaxFragment(Context context) {
        this.context = context;
    }

    public static TaxFragment getInstance(Context context) {
        TaxFragment taxFragment = new TaxFragment(context);

        return taxFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        updateGpsAdapter((int) (Math.random() * 100000));
        updateSearchAdapter("San");
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAdapters();
        gpsList.setAdapter(gpsLoader.getAdapter());
        searchList.setAdapter(searchLoader.getAdapter());
        searchView.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                updateSearchAdapter(newText);
                return false;
            }
        });
    }


    private void initAdapters() {


    }

    private void updateGpsAdapter(Integer zipCode) {
        if (zipCode != null) {
            Query query = new Query()
                    .setUri(TaxDatabaseContentProvider.CONTENT_URI)
                    .setTable("zips")
                    .setColumns(new String[]{"_id", "ZipCode", "TaxRegionName", "CombinedRate", "min(abs(ZipCode-" + zipCode + ")) as distance"})
                    .setGroupBy("TaxRegionName")
                    .setOrderBy("distance")
                    .setLimit("5000");
            gpsLoader = initLoader(context, gpsLoader, GPS_ID, query);
        }
    }

    private void updateSearchAdapter(String searchValue) {
        if (searchValue != null && !searchValue.equals("")) {
            Query query;
            if(isNumeric(searchValue)){
                query = new Query().setUri(TaxDatabaseContentProvider.CONTENT_URI)
                        .setTable("zips")
                        .setColumns(new String[]{"_id", "ZipCode", "TaxRegionName", "CombinedRate"})
                        .setSelection("ZipCode LIKE ?")
                        .setSelectionArgs(new String[]{"%" + searchValue + "%"})
                        .setGroupBy("ZipCode")
                        .setOrderBy("CASE WHEN ZipCode = \"" + searchValue + "%\" THEN 1 " +
                                "ELSE 2 END, " +
                                "ZipCode")
                        .setLimit("5000");
            } else {
                query = new Query()
                        .setUri(TaxDatabaseContentProvider.CONTENT_URI)
                        .setTable("zips")
                        .setColumns(new String[]{"_id", "ZipCode", "TaxRegionName", "CombinedRate"})
                        .setSelection("TaxRegionName LIKE ?")
                        .setSelectionArgs(new String[]{"%" + searchValue + "%"})
                        .setGroupBy("TaxRegionName")
                        .setOrderBy("CASE WHEN TaxRegionName = \"" + searchValue + "\" THEN 1 " +
                                "WHEN TaxRegionName LIKE \"" + searchValue + "%\" THEN 2 " +
                                "ELSE 4 END, " +
                                "TaxRegionName")
                        .setLimit("5000");
            }

            searchLoader = initLoader(context, searchLoader, SEARCH_ID, query);
        }
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    private QueryLoader initLoader(Context context, QueryLoader loader, int id, Query query) {
        if (loader != null) {
            loader.setQuery(query);
            getLoaderManager().restartLoader(id, null, loader);
            return loader;
        } else {
            CursorAdapter adapter = new TaxFormatCursorAdapter(context,
                    R.layout.taxrow,
                    null,
                    new String[]{"ZipCode", "TaxRegionName", "CombinedRate"},
                    new int[]{R.id.taxrow1, R.id.taxrow2, R.id.taxrow3}) {

            };
            return new QueryLoader(context, adapter, getLoaderManager(), id, query);
        }
    }


}
