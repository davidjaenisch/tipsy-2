package com.jae.tipsy.util;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: David
 * Date: 4/20/13
 * Time: 12:21 PM
 * CachedPropertyChangeSupport caches values and puts property change events onto the UI thread.
 */
public class CachedPropertyModel {

    private PropertyChangeSupport support;
    
    private Map<String, Object> cachedValues = new HashMap<String, Object>();

    public CachedPropertyModel() {
        support = new PropertyChangeSupport(this);
    }

    public void firePropertyChange(String propertyName,
                                   Object newValue) {
        Object oldValue = cachedValues.get(propertyName);
        firePropertyChange(propertyName, oldValue, newValue);
    }

    public void firePropertyChange(final PropertyChangeEvent event) {
        cachedValues.put(event.getPropertyName(), event.getNewValue());
        support.firePropertyChange(event);
    }

    public void firePropertyChange(final String propertyName, final boolean oldValue, final boolean newValue) {
        support.firePropertyChange(propertyName, oldValue, newValue);
    }

    public void firePropertyChange(final String propertyName, final int oldValue, final int newValue) {
        support.firePropertyChange(propertyName, oldValue, newValue);
    }

    public void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
        support.firePropertyChange(propertyName, oldValue, newValue);
    }


    public void resetPropertyChangeSupport() {
        support = new PropertyChangeSupport(this);
    }
}
