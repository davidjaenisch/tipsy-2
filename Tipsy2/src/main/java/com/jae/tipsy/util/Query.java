package com.jae.tipsy.util;

import android.net.Uri;

/**
 * Created by David on 11/11/13.
 */
public class Query {

    private Uri uri;

    private String table;

    private String[] columns;
    private String selection;
    private String[] selectionArgs;
    private String groupBy;
    private String having;
    private String orderBy;
    private String limit;

    public Uri getUri() {
        return uri;
    }

    public Query setUri(Uri uri) {
        this.uri = uri;
        return this;
    }

    public String getTable() {
        return table;
    }

    public Query setTable(String table) {
        this.table = table;
        return this;
    }

    public String[] getColumns() {
        return columns;
    }

    public Query setColumns(String[] columns) {
        this.columns = columns;
        return this;
    }

    public String getSelection() {
        return selection;
    }

    public Query setSelection(String selection) {
        this.selection = selection;
        return this;
    }

    public String[] getSelectionArgs() {
        return selectionArgs;
    }

    public Query setSelectionArgs(String[] selectionArgs) {
        this.selectionArgs = selectionArgs;
        return this;
    }

    public String getGroupBy() {
        return groupBy;
    }

    public Query setGroupBy(String groupBy) {
        this.groupBy = groupBy;
        return this;
    }

    public String getHaving() {
        return having;
    }

    public Query setHaving(String having) {
        this.having = having;
        return this;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public Query setOrderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public String getLimit() {
        return limit;
    }

    public Query setLimit(String limit) {
        this.limit = limit;
        return this;
    }
}
