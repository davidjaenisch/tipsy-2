package com.jae.tipsy.util;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by David on 11/8/13.
 */
public abstract class DatabaseContentProvider extends ContentProvider {
    private SQLiteOpenHelper databaseHelper ;
    private final String AUTHORITY;
    private final Uri CONTENT_URI;

    private final String databaseName;
    private final int databaseVersion;

    public DatabaseContentProvider(String authority, String databaseName, int databaseVersion){
        this.databaseName = databaseName;
        this.databaseVersion = databaseVersion;
        this.AUTHORITY = authority;
        CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    }

    @Override
    public boolean onCreate() {
        databaseHelper = new SQLiteAssetHelper(getContext(), databaseName, null, databaseVersion);

        return true;
    }

    @Override
    public int delete(Uri uri, String where, String[] args) {
        String table = getTableName(uri);
        SQLiteDatabase dataBase=databaseHelper.getWritableDatabase();
        return dataBase.delete(table, where, args);
    }

    @Override
    public String getType(Uri arg0) {
        return null;
    }


    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        String table = getTableName(uri);
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        long value = database.insert(table, null, initialValues);
        return Uri.withAppendedPath(CONTENT_URI, String.valueOf(value));
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        String table = getTableName(uri);
        String limit = uri.getQueryParameter("LIMIT");
        String groupBy = uri.getQueryParameter("GROUP BY");
        String having = uri.getQueryParameter("HAVING");
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = database.query(table,  projection, selection, selectionArgs, groupBy, having, sortOrder, limit);
        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String whereClause,
                      String[] whereArgs) {
        String table = getTableName(uri);
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        return database.update(table, values, whereClause, whereArgs);
    }

    public static String getTableName(Uri uri){
        return uri.getQueryParameter("FROM");
    }
}
