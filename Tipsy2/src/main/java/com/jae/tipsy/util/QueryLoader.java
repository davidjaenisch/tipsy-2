package com.jae.tipsy.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;

/**
 * Created by David on 11/6/13.
 */
public class QueryLoader implements LoaderManager.LoaderCallbacks<Cursor> {


    private Context context;
    private CursorAdapter adapter;
    private Query query;

    public QueryLoader(Context context, CursorAdapter adapter, LoaderManager loaderManager, int id, Query query) {
        this.context = context;
        this.adapter = adapter;
        this.query = query;
        loaderManager.initLoader(id, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri queryUri = query.getUri().buildUpon()
                .appendQueryParameter("FROM", query.getTable())
                .appendQueryParameter("GROUP BY", query.getGroupBy())
                .appendQueryParameter("LIMIT", query.getLimit())
                .build();
        return new CursorLoader(context, queryUri,
                query.getColumns(),
                query.getSelection(),
                query.getSelectionArgs(),
                query.getOrderBy());
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (adapter != null) {
            adapter.changeCursor(data);
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (adapter != null) {
            adapter.changeCursor(null);
        }

    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public CursorAdapter getAdapter() {
        return adapter;
    }
}
