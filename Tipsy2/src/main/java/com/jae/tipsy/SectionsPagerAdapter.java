package com.jae.tipsy;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.jae.tipsy.tax.TaxFragment;

/**
 * Created by David on 10/17/13.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private final Context context;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 2:
                return TaxFragment.getInstance(context);
        }
        return TaxFragment.getInstance(context);
    }

    @Override
    public int getCount() {
        return 4;
    }
}
